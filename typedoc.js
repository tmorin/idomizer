module.exports = {
    out: './typedoc',
    name: 'idomizer',
    readme: './README.md',
    externalPattern: 'node\\_modules',
    excludePrivate: true,
    excludeProtected: true,
    target: 'ES6'
};
